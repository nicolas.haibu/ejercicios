const clients = [
    { id: 1, rut: "89873550", name: "LARA RENE PETTY BERGER" },
    { id: 2, rut: "86833361", name: "CONWAY LANDRY POLLARD BRADLEY" },
    { id: 3, rut: "88271452", name: "MICHELLE LETITIA BATTLE MOONEY" },
    { id: 4, rut: "87252013", name: "SIMMONS NELSON WITT MONROE" },
    { id: 5, rut: "81706494", name: "BRADY MARY RANDALL FERNANDEZ" },
    { id: 6, rut: "71167355", name: "ACOSTA COLE ATKINSON PITTS" },
    { id: 7, rut: "79093176", name: "DOMINGUEZ HOUSE GONZALES SALAZAR" },
    { id: 8, rut: "91361017", name: "KIRSTEN COLLINS BYERS COFFEY" },
    { id: 9, rut: "9065642K", name: "FIELDS RATLIFF MORRIS QUINN" }
];

const banks = [
    { id: 1, name: 'SCOTIABANK' },
    { id: 2, name: 'BCI' },
    { id: 3, name: 'ITAU' },
    { id: 4, name: 'CONDELL' },
];

const accounts = [
    { clientId: 4, bankId: 1, balance: 79069 },
    { clientId: 6, bankId: 3, balance: 136060 },
    { clientId: 9, bankId: 3, balance: 74908 },
    { clientId: 2, bankId: 2, balance: 4391 },
    { clientId: 6, bankId: 2, balance: 116707 },
    { clientId: 1, bankId: 3, balance: 157627 },
    { clientId: 5, bankId: 4, balance: 136372 },
    { clientId: 7, bankId: 4, balance: 190204 },
    { clientId: 5, bankId: 4, balance: 149670 },
    { clientId: 2, bankId: 3, balance: 143321 },
    { clientId: 2, bankId: 4, balance: 67466 },
    { clientId: 2, bankId: 3, balance: 17956 },
    { clientId: 9, bankId: 2, balance: 43194 },
    { clientId: 5, bankId: 1, balance: 52245 },
    { clientId: 6, bankId: 2, balance: 41562 },
    { clientId: 3, bankId: 2, balance: 138046 },
    { clientId: 6, bankId: 3, balance: 196964 },
    { clientId: 8, bankId: 3, balance: 73803 },
    { clientId: 9, bankId: 2, balance: 150402 },
    { clientId: 7, bankId: 1, balance: 122869 },
    { clientId: 5, bankId: 4, balance: 65223 },
    { clientId: 7, bankId: 3, balance: 143589 },
    { clientId: 9, bankId: 3, balance: 43346 },
    { clientId: 2, bankId: 1, balance: 60421 },
    { clientId: 4, bankId: 4, balance: 184110 },
    { clientId: 8, bankId: 4, balance: 195903 },
    { clientId: 5, bankId: 2, balance: 77649 },
    { clientId: 8, bankId: 4, balance: 28170 },
    { clientId: 5, bankId: 1, balance: 132850 },
    { clientId: 1, bankId: 3, balance: 139679 },
    { clientId: 7, bankId: 4, balance: 119808 },
    { clientId: 4, bankId: 4, balance: 109201 },
    { clientId: 9, bankId: 3, balance: 112529 },
    { clientId: 1, bankId: 3, balance: 137914 },
    { clientId: 6, bankId: 2, balance: 122904 },
    { clientId: 5, bankId: 4, balance: 103142 },
    { clientId: 8, bankId: 2, balance: 69163 },
    { clientId: 2, bankId: 4, balance: 57812 },
    { clientId: 2, bankId: 3, balance: 32851 },
    { clientId: 7, bankId: 1, balance: 109763 },
    { clientId: 8, bankId: 3, balance: 147442 },
    { clientId: 9, bankId: 1, balance: 42217 },
    { clientId: 1, bankId: 1, balance: 39658 },
    { clientId: 6, bankId: 2, balance: 8664 },
    { clientId: 8, bankId: 1, balance: 41915 },
    { clientId: 7, bankId: 1, balance: 31879 },
    { clientId: 7, bankId: 4, balance: 117795 },
    { clientId: 1, bankId: 4, balance: 108862 },
    { clientId: 5, bankId: 1, balance: 18550 },
];

//console.log("CLIENTES" + JSON.stringify(clients));
//console.log("BANKO" +  JSON.stringify(banks));
//console.log("cuentas" + JSON.stringify(accounts));


console.log("*****************EJERCICIO 0******************");
/**
Ejercicio 0
@description Retornar un arreglo con los ID de los clientes
*/

var dats = clients.map(function(x) {
    return x.id;
});
 
console.log(dats);



console.log("*****************EJERCICIO 1******************");
/**
Ejercicio 1
@description  Retornar un arreglo con los ID de los clientes ordenados por RUT mayo a menor
*/


/* sin ultimo diigito */

var seg = clients.sort(function(a, b){
    return b.rut - a.rut;
});
var filtro = seg.filter(function(a){
  console.log(a.id, a.rut.substring(0, a.rut - 1));
  //console.log(a.rut);  
});

/* con ultimo diigito */


var seg = clients.sort(function(a, b){
    return b.rut - a.rut;
});
var filtro = seg.filter(function(a){
  console.log(a.id, a.rut);
  //console.log(a.rut);  
});






console.log("*****************EJERCICIO 2******************");
/**
Ejercicio 2
@description Retornar un arreglo con los nombres de los clientes, ordenados de mayor a menor
             por la suma TOTAL de los saldos de las Cuentas
*/
            

    var suma = 0;

    for (let index = 0; index < accounts.length; index++) {
        const element = accounts[index].clientId;
        const total = accounts[index].balance;
       // console.log(element);

       for (let indice = 0; indice < clients.length; indice++) {
           const elemento = clients[indice].id;
           if (elemento == element) {             
               suma += total;
           }
        
       }
      
    } 

     

var ter = accounts.sort(function(a, b){   
  return b.balance - a.balance;
})

var filtro = ter.filter(function(a){
    if(a.clientId == 1){
      console.log("LARA RENE PETTY BERGER");
    }if(a.clientId == 2){
        console.log("CONWAY LANDRY POLLARD BRADLEY");

    }if(a.clientId == 3){
        console.log("MICHELLE LETITIA BATTLE MOONEY");

    }if(a.clientId == 4){
        console.log("SIMMONS NELSON WITT MONROE");

    }if(a.clientId == 5){
        console.log("BRADY MARY RANDALL FERNANDEZ");

    }if(a.clientId == 6){
        console.log("ACOSTA COLE ATKINSON PITTS");

    }if(a.clientId == 7){
        console.log("DOMINGUEZ HOUSE GONZALES SALAZAR");

    }if(a.clientId == 8){
        console.log("KIRSTEN COLLINS BYERS COFFEY");

    }if(a.clientId == 9){
        console.log("IELDS RATLIFF MORRIS QUINN");

    }
 
    console.log(a.clientId, a.balance);

    //console.log(a.rut);  
  });




console.log("*****************EJERCICIO 3******************");
/**
Ejercicio 3
@description  Devuelve un objeto cuyo índice es el nombre de los bancos
              y cuyo valor es un arreglo de los ruts de los clientes ordenados alfabéticamente por 'nombre'
*/

var tercero = clients.sort(function(a, b){   
    var nombre = a.name;
    var segundoNombre = b.name;
    return (nombre < segundoNombre) ? -1 : (nombre > segundoNombre) ? 1 : 0;
});

var filtro = tercero.filter(function(x){
      
      
    const primero = { a: banks[0].name, b: x.name};
    const segundo = { a: banks[1].name, b: x.name};
    const tercer = { a: banks[2].name, b: x.name};
    const cuart = { a: banks[3].name, b: x.name};

    console.log(primero);
    console.log(segundo);
    console.log(tercer);
    console.log(cuart);

});






console.log("*****************EJERCICIO 4******************");

/**
Ejercicio 4
@description   Devuelve un arreglo ordenado de mayor a menor con el saldo de los clientes que
               tengan más de 25000 en el banco 'SCOTIABANK'
*/


var cuarto = accounts.sort(function(a, b){   
        return b.balance - a.balance;

    
});

var filtro = cuarto.filter(function(a){
   if(a.balance >= 25000 && a.bankId == 1){
       console.log(a.balance);
       
   }
});
//console.log(cuarto);

console.log("*****************EJERCICIO 5******************");

/**
Ejercicio 5
@description  Devuelve un arreglo con la 'id' de los Bancos de menor a mayor por el
             TOTAL de dinero que administran en las cuentas de sus clientes
*/
var quinto = accounts.sort(function(a, b){   
    return a.balance - b.balance;

});

var filtro  = quinto.filter(function(x){
  console.log(x.bankId, x.balance);
  
});

//console.log(quinto);


console.log("*****************EJERCICIO 6******************");

/**
Ejercicio 6
@description  Devuelve un objeto en donde la key son los nombre de los bancos
              y el valor es la cantidad de clientes que solo tienen una cuenta en ese banco
*/


const totalBank1Accounts = accounts.filter(obj => obj.bankId === 1).length;
const totalBank2Accounts = accounts.filter(obj => obj.bankId === 2).length;
const totalBank3Accounts = accounts.filter(obj => obj.bankId === 3).length;
const totalBank4Accounts = accounts.filter(obj => obj.bankId === 4).length;


//console.log(totalBank1Accounts);
//console.log(totalBank2Accounts);
//console.log(totalBank3Accounts);
//console.log(totalBank4Accounts);







const primero = { a: banks[0].name, b: totalBank1Accounts};
const segundo = { a: banks[1].name, b: totalBank2Accounts};
const tercer = { a: banks[2].name, b: totalBank3Accounts};
const cuart = { a: banks[3].name, b: totalBank4Accounts};

console.log(primero);
console.log(segundo);
console.log(tercer);
console.log(cuart);



console.log("*****************EJERCICIO 7******************");

/**
Ejercicio 7
@description  Devuelve un objeto en donde la key son el nombre de los bancos
              y el valor es el 'id' de su cliente con menos dinero.
*/

const sacando1Id = accounts.filter(obj => obj.bankId === 1);
const sacando2Id = accounts.filter(obj => obj.bankId === 2);
const sacando3Id = accounts.filter(obj => obj.bankId === 3);
const sacando4Id = accounts.filter(obj => obj.bankId === 4);



var resultUno = sacando1Id.reduce(function(res, obj) {
    return (obj.bankId < res.bankId) ? obj : res;
});

var resultDos = sacando2Id.reduce(function(res, obj) {
    return (obj.bankId < res.bankId) ? obj : res;
});

var resultTres = sacando3Id.reduce(function(res, obj) {
    return (obj.bankId < res.bankId) ? obj : res;
});

var resultCuatro = sacando4Id.reduce(function(res, obj) {
    return (obj.bankId < res.bankId) ? obj : res;
});

//console.log(resultUno);
//console.log(resultDos);
//console.log(resultTres);
//console.log(resultCuatro);






const primer = { a: banks[0].name, b: resultUno.clientId };
const segund = { a: banks[1].name, b: resultDos.clientId };
const terce = { a: banks[2].name, b: resultTres.clientId };
const cuar = { a: banks[3].name, b: resultCuatro.clientId };

console.log(primer);
console.log(segund);
console.log(terce);
console.log(cuar);